const express = require('express')
const bodyparser = require('body-parser')
const { rmSync } = require('fs')
const app = express()
const port = 5000

app.use(bodyparser.json())

// CORS Middleware
app.use(function (req, res, next) {
    // Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`))

app.get('/calculator', function (req, res) {
    res.json("My Calculator Services v1.0");
});

app.post('/calculator', function (req, res){
    console.log('In Post /calculator Route with post of ' + JSON.stringify(req.body));
    if(req.body.num1 == null || req.body.num2 == null || req.body.operator == null){
        res.status(400).json({error: "Invalid calculator input"});
    } else {
        let x = parseInt(req.body.num1)
        let y = parseInt(req.body.num2)
        let operator = req.body.operator

        if(x == 0 && operator == "/"){
            res.status(200).json({success: "infinity"});
        } else if(y == 0 && operator == "/") {
            res.status(200).json({successs: "cannot divide by zero"});
        } else {
            let z = eval(x + operator + y);
            res.status(200).json({success: z});
        }
    }
});