export class CalculatorModel {
    private firstOperand:number;
    private secondOperand:number;
    private operator:string;

    constructor(){
        this.firstOperand = 0;
        this.secondOperand = 0;
        this.operator = "Add";
    }

    get FirstOperand():number{
        return this.firstOperand;
    }

    set FirstOperand(firstOperand:number){
        this.firstOperand = firstOperand;
    }

    get SecondOperand():number {
        return this.secondOperand;
    }

    set SecondOperand(secondOperand:number){
        this.secondOperand = secondOperand;
    }

    get Operator():string {
        return this.operator;
    }

    set Operator(operator:string){
        this.operator = operator;
    }
}