import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CalculatorModel } from '../models/CalculatorModel';


@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  @Input() calculator:CalculatorModel = new CalculatorModel();
  operators:string[] = [];
  error:string="";

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.operators = ["Add", "Subtract", "Multiply", "Divide"];
    this.error = "";
  }

  calculate() {
    this.error = "";
    if(isNaN(this.calculator.FirstOperand) || isNaN(this.calculator.SecondOperand)){
      console.log("Invalid input type error");
      this.error = "There is a non number value in one of the operands. Please validate all operands are numbers.";
    } else if (this.calculator.FirstOperand == 0 && this.calculator.Operator == "Divide") {
      this.error = "Error: Cannot divide 0 by any number. The result will be inifinite.";
    } else if(this.calculator.SecondOperand == 0 && this.calculator.Operator == "Divide"){
      console.log("Divide by zero error")
      this.error = "Error: Cannot divide by 0. Please enter a non zero number in the denominator to divide by.";
    } else {
      console.log("Calculating results");
      if(this.calculator.Operator == "Add"){
        this.calculator.Operator = "+";
      } else if(this.calculator.Operator == "Subtract"){
        this.calculator.Operator = "-";
      } else if(this.calculator.Operator == "Multiply"){
        this.calculator.Operator = "*";
      } else if(this.calculator.Operator == "Divide"){
        this.calculator.Operator = "/";
      }
      let results = Number.parseFloat(eval(this.calculator.FirstOperand + " " + this.calculator.Operator + " " + this.calculator.SecondOperand));
      console.log("Results: " + results);
      this.router.navigate(['result'], {queryParams: {results: results}});
    }
  }
}
